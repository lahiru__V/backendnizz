const e = require("cors");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const user = new Schema(
  {
    name: {
      type: String,
      default: "",
    },
    email: {
      type: String,
      default: "",
    },
    password: {
      type: String,
      default: "",
    },
    phone: {
      type: String,
      default: "",
    },
    dob: {
      type: String,
      default: "",
    },
    image: {
      type: String,
      default: "",
    },
    education: {
      type: Array,
      default: [],
    },
    skills: {
      type: Array,
      default: [],
    },
    experience: {
      type: Array,
      default: [],
    },
  },
  {
    timestamps: true,
  }
);
const userSchema = mongoose.model("user", user);
module.exports = userSchema; 
