const adminSchema = require('../models/admin');
const bcrypt = require('bcryptjs');
const express = require('express');
const router = express.Router();

// POST: Register a new admin
router.post('/register', async (req, res) => {
    try {
        const { name, type, dob, email, phone, companyInformation, contactInformation, workExperience, expertise, password, image } = req.body;

        // Check if the email already exists
        const existingUser = await adminSchema.findOne({ email });
        if (existingUser) {
            return res.status(409).json({ message: 'Email is already used' });
        }

        // Hash the password
        const hashedPassword = await bcrypt.hash(password, 10);

        // Create new admin
        const admin = new adminSchema({
            name,
            type,
            dob,
            email,
            phone,
            companyInformation,
            contactInformation,
            workExperience,
            expertise,
            password: hashedPassword,
            image
        });

        // Save the admin
        await admin.save();
        res.status(201).json({ message: 'Admin member added successfully' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

// POST: Admin login
router.post('/login', async (req, res) => {
    try {
        const { email, password } = req.body;

        // Find the user by email
        const existingUser = await adminSchema.findOne({ email });
        if (!existingUser) {
            return res.status(401).json({ message: 'Email or password is incorrect' });
        }

        // Check the password
        const passwordMatch = await bcrypt.compare(password, existingUser.password);
        if (!passwordMatch) {
            return res.status(401).json({ message: 'Email or password is incorrect' });
        }

        // Respond success on correct credentials
        res.status(200).json({ message: 'Login successful', existingUser });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

// GET: Retrieve all admin users
router.get('/getAll', async (req, res) => {
    try {
        // Select all but exclude password field
        const admins = await adminSchema.find();
        res.status(200).json(admins);
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

// GET: Retrieve an admin by ID
router.get('/get/:id', async (req, res) => {
    try {
        // Extract the ID from the request parameters
        const { id } = req.params;

        // Find the admin by ID in the database
        const admin = await adminSchema.findById(id);
        if (!admin) {
            return res.status(404).json({ message: 'Admin not found' });
        }

        // Return the found admin
        res.status(200).json(admin);
    } catch (err) {
        if (err.kind === 'ObjectId') {
            return res.status(400).json({ message: 'Invalid ID format' });
        }
        res.status(500).json({ error: err.message });
    }
});

// Generate unique ID
function generateId() {
    return new Date().getTime().toString(36) + Math.random().toString(36).slice(2);
}

// PUT endpoint to add or update details in an admin document
router.put("/addDetail/:id", async (req, res) => {
    const adminId = req.params.id;
    const detailType = req.body.type;
    const detailData = req.body.data;

    try {
        // Find the admin document by ID
        const admin = await adminSchema.findById(adminId);
        if (!admin) {
            return res.status(404).json({ message: "Admin not found" });
        }

        let message = "";
        switch (detailType) {
            case "companyInformation":
            case "workExperience":
            case "expertise":
                // Add entry to an array with a unique ID
                const uniqueId = generateId();
                admin[detailType].push({ ...detailData, id: uniqueId });
                message = `${detailType.slice(0, -1)} added successfully`;
                break;
            case "contactInformation":
                // Update or set the contact information object
                admin.contactInformation = { ...admin.contactInformation, ...detailData };
                message = "Contact information updated successfully";
                break;
            default:
                return res.status(400).json({ message: "Invalid detail type" });
        }

        // Save the updated admin document
        await admin.save();
        return res.status(200).json({ message: message });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Internal server error" });
    }
});

router.put("/deleteDetail/:id", async (req, res) => {
    const adminId = req.params.id;
    const detailType = req.body.type;
    const detailId = req.body.id; // Used for array types
    const field = req.body.field; // Used for object field deletions

    try {
        const admin = await adminSchema.findById(adminId);
        if (!admin) {
            return res.status(404).json({ message: "Admin not found" });
        }

        let message = "";
        if (['companyInformation', 'workExperience', 'expertise'].includes(detailType)) {
            // Handle deletion for array types
            admin[detailType] = admin[detailType].filter(item => item.id !== detailId);
            message = `${detailType.slice(0, -1)} deleted successfully`;
        } else if (detailType === 'contactInformation' && field) {
            // Handle deletion of specific field in contactInformation
            if (admin.contactInformation.hasOwnProperty(field)) {
                delete admin.contactInformation[field];
                message = `${field} removed from contact information successfully`;
            } else {
                return res.status(404).json({ message: "Field not found in contact information" });
            }
        } else {
            return res.status(400).json({ message: "Invalid detail type or field" });
        }

        await admin.save();
        return res.status(200).json({ message });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Internal server error" });
    }
});

// DELETE an admin by ID
router.delete('/delete/:id', async (req, res) => {
    const { id } = req.params;

    try {
        // Attempt to find and remove the admin by ID
        const admin = await adminSchema.findByIdAndDelete(id);

        // If no admin found, send a 404 response
        if (!admin) {
            return res.status(404).json({ message: "Admin not found" });
        }

        // Send back a success message
        res.status(200).json({ message: "Admin deleted successfully" });
    } catch (error) {
        // Log the error and return a 500 response
        console.error(error);
        return res.status(500).json({ error: "Internal server error" });
    }
});

// PUT: Update an entire admin by ID, with password hashing
router.put('/update/:id', async (req, res) => {
    const { id } = req.params;
    const adminUpdates = req.body;
  
    try {
      // If a new password is provided, hash it
      if (adminUpdates.password) {
        const hashedPassword = await bcrypt.hash(adminUpdates.password, 10);
        adminUpdates.password = hashedPassword;
      }
  
      // Find the admin by ID and update it with the new data
      // { new: true } option returns the document after update was applied
      const updatedAdmin = await adminSchema.findByIdAndUpdate(id, adminUpdates, { new: true, runValidators: true });
  
      // If no admin found, send a 404 response
      if (!updatedAdmin) {
        return res.status(404).json({ message: "Admin not found" });
      }
  
      // Send back the updated admin data, excluding the password field
      const result = updatedAdmin.toObject();
      delete result.password;
      res.status(200).json(result);
    } catch (error) {
      // Handle possible errors
      console.error(error);
      return res.status(500).json({ error: "Internal server error" });
    }
  });

module.exports = router;
